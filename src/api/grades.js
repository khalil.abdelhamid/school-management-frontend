import axios from 'axios';


const url = 'http://localhost:3000/api/v1/grades';

const instance = axios.create({
  baseURL: url,
  timeout: 2000,
  headers: {
    'Authorization': `Bearer ${localStorage.getItem('token') || ''}`,
  }
})


const getAll = () => instance.get().then(res => res.data);

const api = {
  instance, getAll
}

export default api;