import axios from 'axios'

const url = 'http://localhost:3000/api/v1/lessons'

const instance = axios.create({
  baseURL: url,
  timeout: 3000,
  headers: {
    'Authorization': `Bearer ${localStorage.getItem('token') || ''}`,
  }
})

const deleteLesson = (id) =>
  instance.delete(`http://localhost:3000/api/v1/lessons/${id}`)

const api = {
  instance,
  delete: deleteLesson,
}

export default api
