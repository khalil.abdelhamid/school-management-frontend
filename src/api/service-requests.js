import axios from 'axios'

const url = 'http://localhost:3000/api/v1/service-requests'
const instance = axios.create({
  baseURL: 'http://localhost:3000/api/v1/service-requests',
  timeout: 2000,
  headers: {
    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
  },
})

const getAllRequests = () => instance.get().then((res) => res.data)
const createRequest = (title, description) =>
  instance.post(url, { title, description }).then(res => res.data)
const deleteRequest = (id) =>
  instance.delete(`http://localhost:3000/api/v1/service-requests/${id}`)

const api = {
  instance,
  getAll: getAllRequests,
  delete: deleteRequest,
  create: createRequest,
}
export default api
