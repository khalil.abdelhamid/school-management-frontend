import axios from 'axios'

const url = 'http://localhost:3000/authenticate'

const instance = axios.create({
  baseURL: url,
})

const authenticate = (email, password) =>
  axios
    .post('http://localhost:3000/authenticate', { email, password })
    .then((res) => res.data)

const api = {
  instance,
  authenticate,
}

export default api
