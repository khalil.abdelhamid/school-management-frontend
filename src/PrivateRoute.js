import React from 'react'

import { Redirect, Route } from 'react-router-dom'
import useAuth from './hooks/useAuth'

const PrivateRoute = ({ component, ...rest }) => {
  const [isAuthenticated] = useAuth()

  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isAuthenticated()) return <Redirect to="/login" />
        return React.createElement(component, props)
      }}
    />
  )
}

export default PrivateRoute
