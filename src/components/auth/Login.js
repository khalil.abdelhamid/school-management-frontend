import React, { useEffect, useState } from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import useAuth from '../../hooks/useAuth'
import { useHistory } from 'react-router-dom'

import '../../assets/style/Login.css'

const Login = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const [token, loading, login, logout] = useAuth()
  const history = useHistory()

  const signin = () => {
    if (areInputsValid()) {
      login(email, password)
        .then((_) => history.push('/student'))
        .catch((err) => setError('Failed to authenticate'))
    }
  }

  const areInputsValid = () => {
    if (!email || !password) {
      setError('Remplire les champs necessaire')
      return false
    }

    return true
  }

  return (
    <Container>
      <Row>
        <Col>
          <form id="msform">
            <fieldset>
              <h2 class="fs-title">Authentifier</h2>
              <h3 class="fs-subtitle">
                {error && <span class="text-danger">{error}</span>}
              </h3>
              <input
                type="text"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <input
                type="password"
                placeholder="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <input
                type="button"
                className="next action-button"
                value="Login"
                variant="primary"
                onClick={signin}
              />
            </fieldset>
          </form>
        </Col>
      </Row>
    </Container>
  )
}

export default Login
