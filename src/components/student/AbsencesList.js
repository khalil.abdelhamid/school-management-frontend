import React, { useState, useEffect, useRef, createRef } from 'react'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import AbsencesTable from './AbsencesTable'
import AbsencesAPI from '../../api/absences'
import Loading from '../commons/Loading'
import Table from 'react-bootstrap/Table'
import Notification from '../commons/Notification'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Chart } from 'chart.js'

const AbsencesList = () => {
  const [error, setError] = useState('')
  const [showNotification, setShowNotification] = useState(false)
  const [loading, setLoading] = useState(true)
  const [absences, setAbsences] = useState([])
  const [chart, setChart] = useState({ label: [], data: [] })
  const canvasRef = createRef()

  useEffect(() => {
    AbsencesAPI.getAll()
      .then((res) => {
        res.sort((first, second) => first.absenceDate > second.absenceDate)
        setAbsences(res)
        console.log({ absences: absences })
        setLoading(false)

        const result = filterAbsencesByCourse()
        setChart({
          labels: result.map((item) => item.course.title),
          data: result.map((item) => item.absences.length),
        })
        setupChart()
      })
      .catch((err) => {
        console.error(error)
        setError('Failed to load data try, later')
        setShowNotification(true)
        setLoading(false)
      })
  }, [])

  const filterAbsencesByCourse = () => {
    const courses = new Set(absences.map((absence) => absence.course.id))
    const result = []

    courses.forEach((id) => {
      const currentAbsences = absences.filter(
        (absence) => absence.course.id === id
      )
      const currentCourse = currentAbsences[0].course
      result.push({ absences: currentAbsences, course: currentCourse })
      console.info({ result })
    })

    return result
  }

  const setupChart = () => {
    const mychart = new Chart('absences-chart', {
      type: 'pie',
      data: {
        labels: chart.labels,
        datasets: [{ data: chart.data }],
      },
    })
  }

  return loading ? (
    <Loading />
  ) : (
    <Container>
      <Row>
        <Col>
          <Notification
            message={error}
            show={showNotification}
            onClose={(_) => setShowNotification(false)}
            isError={true}
          />
          <Tabs defaultActiveKey="absences">
            <Tab eventKey="absences" title="Liste des absences">
              <AbsencesTable absences={absences} />
            </Tab>
            <Tab eventKey="bycourse" title="Absences par module">
              <Row>
                <Col>
                  <Table hover striped bordered>
                    <thead>
                      <tr>
                        <th>Module</th>
                        <th>total des absences</th>
                      </tr>
                    </thead>
                    <tbody>
                      {filterAbsencesByCourse().map((item) => (
                        <tr key={item.course.id}>
                          <td>{item.course.title}</td>
                          <td>{item.absences.length}</td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </Col>
                {/* TODO: add chart.js */}
                <Col>
                  <canvas
                    ref={canvasRef}
                    id="absences-chart"
                    style={{
                      width: '100%',
                      height: 'auto',
                    }}
                  ></canvas>
                </Col>
              </Row>
            </Tab>
          </Tabs>
        </Col>
      </Row>
    </Container>
  )
}

export default AbsencesList
