import React, { useState, useEffect, useMemo } from 'react'
import CoursesAPI from '../../api/courses'
import Table from 'react-bootstrap/Table'
import Loading from '../commons/Loading'
import Notification from '../commons/Notification'
import FormControl from 'react-bootstrap/FormControl';

const CoursesTable = () => {
  const [loading, setloading] = useState(true)
  const [error, setError] = useState('')
  const [showNotification, setShowNotification] = useState(false)
  const [courses, setCourses] = useState([])
  const [keyword, setKeyword] = useState('')

  const filteredCourse = () => {
    if (!keyword) return courses

    return courses.filter((course) => {
      if (course.title.includes(keyword)) return true
      if (course.professor.firstName.includes(keyword)) return true
      if (course.professor.lastName.includes(keyword)) return true
      return false
    })
  }

  useEffect(() => {
    CoursesAPI.getAll()
      .then((res) => {
        console.log({ course: res })
        setCourses(res)
        setloading(false)
      })
      .catch((err) => {
        console.error({ loadCourse: err })
        setError('Failed to load data')
        setShowNotification(true)
      })
  }, [])

  return loading ? (
    <Loading />
  ) : (
    <div>
      <Notification
        message={error}
        isError={true}
        show={showNotification}
        onClose={(_) => setShowNotification(false)}
      />
      <FormControl
        className="my-2"
        type="text"
        placeholder="Filtrer par module ou professeur"
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
      />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Module</th>
            <th>Cours</th>
            <th>Professeur</th>
            <th>Nombre de cours</th>
          </tr>
        </thead>
        <tbody>
          {filteredCourse().map((course) => (
            <tr key={course.id}>
              <td>{course.title}</td>
              <td>
                {course.lessons.map((lesson) => (
                  <div>
                    <a
                      key={lesson.id}
                      href={`/lessons/${lesson.id}/download`}
                      target="_blank"
                    >
                      {lesson.title}
                    </a>
                  </div>
                ))}
              </td>
              <td>{`${course.professor.firstName} ${course.professor.lastName}`}</td>
              <td>{course.lessons.length} Cours</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  )
}

export default CoursesTable
