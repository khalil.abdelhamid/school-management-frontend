import React, { useRef, useState, useEffect } from 'react'
import Table from 'react-bootstrap/Table'
import GradesAPI from '../../api/grades'
import Loading from '../commons/Loading'
import Notification from '../commons/Notification'
import FormControl from 'react-bootstrap/FormControl'

const GradesTable = () => {
  const [error, setError] = useState('')
  const [showNotification, setShowNotification] = useState(false)
  const [grades, setGrades] = useState([])
  const [loading, setLoading] = useState(true)
  const [keyword, setKeyword] = useState('')

  useEffect(() => {
    setLoading(true)
    setError('')
    GradesAPI.getAll()
      .then((res) => {
        setGrades(res)
        setLoading(false)
      })
      .catch((err) => {
        console.error(err)
        setError('Failed to load data')
        setShowNotification(true)
        setLoading(false)
      })
  }, [])

  const filterGrades = () => {
    if (!keyword) return grades

    return grades.filter((grade) => grade.course.title.includes(keyword))
  }

  return loading ? (
    <Loading />
  ) : (
    <div>
      <Notification
        message={error}
        show={showNotification}
        onClose={(_) => setShowNotification(false)}
        isError={true}
      />
      <FormControl
        className="my-2"
        type="text"
        placeholder="Rechercher un module"
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
      />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Note</th>
            <th>Status</th>
            <th>Module</th>
          </tr>
        </thead>
        <tbody>
          {filterGrades().map((grade) => (
            <tr key={grade.id}>
              <td>{grade.value}</td>
              <td className={grade.status.trim().toLowerCase() === 'v' ? 'text-success fw-bold' : 'text-danger fw-bold'}>
                {grade.status}
              </td>
              <td>{grade.course.title}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  )
}

export default GradesTable
