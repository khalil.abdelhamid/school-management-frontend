import React, { useState, useEffect } from 'react'
import Table from 'react-bootstrap/Table'
import AbsencesAPI from '../../api/absences'
import Loading from '../commons/Loading'
import FormControl from 'react-bootstrap/FormControl';

const AbsencesTable = ({absences}) => {
  const [keyword, setKeyword] = useState('')
  const filterAbsences = () => {
    if (!keyword) return absences

    return absences.filter((absence) => absence.course.title.includes(keyword))
  }


  return (
    <div>
      <FormControl
        className="my-2"
        type="text"
        placeholder="Filtrer par module"
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
      />
      <Table bordered striped hover>
        <thead>
          <tr>
            <th>Date absences</th>
            <th>Module</th>
            <th>Semestre</th>
          </tr>
        </thead>
        <tbody>
          {filterAbsences().map((absence) => (
            <tr key={absence.id}>
              <td>{absence.absenceDate}</td>
              <td>{absence.course.title}</td>
              <td>{absence.course.semester}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  )
}

export default AbsencesTable
