import React, { useState, useEffect } from 'react'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import Loading from '../commons/Loading'
import ServiceRequestsAPI from '../../api/service-requests'
import CreateServiceRequestModal from '../modals/CreateServiceRequestModal'
import Notification from '../commons/Notification'

const ServiceRequestTable = () => {
  const [serviceRequests, setServiceRequests] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState('')
  const [isModalShowen, setIsModalShowen] = useState(false)
  const [notificationShow, setNotificationShow] = useState(false)

  useEffect(() => {
    setLoading(true)
    setError('')
    ServiceRequestsAPI.getAll()
      .then((res) => {
        setServiceRequests(res)
        setLoading(false)
        console.log({ requests: res })
      })
      .catch((err) => {
        console.error(err)
        setLoading(false)
        setError('Failed to load data')
        setNotificationShow(true)
      })
  }, [])

  const deleteRequest = (id) => {
    ServiceRequestsAPI.delete(id)
      .then((res) =>
        setServiceRequests(serviceRequests.filter((req) => req.id !== id))
      )
      .catch((err) => {
        console.error(err)
        setError('Failed to delete item ' + id)
        setNotificationShow(true)
      })
  }

  const createRequest = (title, description) => {
    setError('')
    ServiceRequestsAPI.create(title, description)
      .then((res) => {
        console.log({created: res})
        setServiceRequests(serviceRequests.concat(res))
        setIsModalShowen(false)
      })
      .catch((err) => {
        console.error(err)
        setError("Demande d'un nouveau service a echoue, ressayer plus tard")
        setNotificationShow(true)
      })
  }

  const showmodal = (e) => {
    e.preventDefault()
    setIsModalShowen(true)
  }
  const hidemodal = () => setIsModalShowen(false)

  return loading ? (
    <Loading />
  ) : (
    <div>
      <Notification
        show={notificationShow}
        onClose={(_) => setNotificationShow(false)}
        message={error}
        isError={true}
      />
      <div className="px-4 py-1">
        <span
          style={{
            textDecoration: 'underline',
            cursor: 'pointer',
          }}
          href="#"
          onClick={(e) => showmodal(e)}
        >
          Demander un service
        </span>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Titre</th>
            <th>Description</th>
            <th>Date de Creation</th>
            <th>---</th>
          </tr>
        </thead>
        <tbody>
          {serviceRequests.map((request) => (
            <tr key={request.id}>
              <td>{request.title}</td>
              <td>{request.description}</td>
              <td>{request.createdDate}</td>
              <td>
                <Button
                  onClick={() => deleteRequest(request.id)}
                  variant="danger"
                >
                  Supprimer
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <CreateServiceRequestModal
        show={isModalShowen}
        onHide={hidemodal}
        onSave={createRequest}
      />
    </div>
  )
}

export default ServiceRequestTable
