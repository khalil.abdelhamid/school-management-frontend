import React from 'react'
import Button from 'react-bootstrap/Button'
import { useHistory } from 'react-router-dom'
import '../../assets/style/Sidebar.css';

const Sidebar = () => {
  const history = useHistory()

  const route = (path) => history.push(path)

  return (
    <nav className="main-menu expanded">
      <ul>
        <li>
          <a
            onClick={(e) => {
              e.preventDefault()
              route('/student/requests')
            }}
          >
            <i className="fa fa-home fa-2x"></i>
            <span className="nav-text">Demander un service</span>
          </a>
        </li>
        <li className="has-subnav">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault()
              route('/student/grades')
            }}
          >
            <i className="fa fa-laptop fa-2x"></i>
            <span className="nav-text">Note des module</span>
          </a>
        </li>
        <li className="has-subnav">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault()
              route('/student/absences')
            }}
          >
            <i className="fa fa-table fa-2x"></i>
            <span className="nav-text">Absences</span>
          </a>
        </li>
        <li className="has-subnav">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault()
              route('/student/courses')
            }}
          >
            <i className="fa fa-folder-open fa-2x"></i>
            <span className="nav-text">Module / Cours</span>
          </a>
        </li>
      </ul>
    </nav>
  )
}

export default Sidebar
