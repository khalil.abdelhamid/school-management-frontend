import React, { useState, useEffect, useRef } from 'react'
import ServiceRequestAPI from '../../api/service-requests'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import Loading from '../commons/Loading'
import Whoops from '../commons/Whoops'
import FormControl from 'react-bootstrap/FormControl';

const ServiceRequestsList = () => {
  const [keyword, setKeyword] = useState('')
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState('')
  const [requests, setRequests] = useState([])

  useEffect(() => {
    setError('')
    ServiceRequestAPI.getAll()
      .then((res) => {
        setRequests(res)
        console.log({ requests })
        setLoading(false)
      })
      .catch((err) => {
        console.error(error)
        setError('Failed to load data')
        setLoading(false)
      })
  }, [])

  const filterRequests = () => {
    if (!keyword) return requests

    return requests.filter((req) => {
      if (req.title.includes(keyword)) return true
      if (req.description.includes(keyword)) return true
      if (req.student.lastName.includes(keyword)) return true
      if (req.student.firstName.includes(keyword)) return true
      return false
    })
  }

  const changeRequestStatus = (req, isAccepted) => {
    console.log('IMPLEMENT THIS METHOD, CHANGE REQUEST STATUS')
  }

  if (error) return <Whoops message={error.message} />

  return loading ? (
    <Loading />
  ) : (
    <div>
      <FormControl
        type="text"
        placeholder="Filtrer par titre, etudiant"
        value={keyword}
        className="my-2"
        onChange={(e) => setKeyword(e.target.value)}
      />
      <Table hover striped bordered>
        <thead>
          <tr>
            <th>Titre</th>
            <th>Description</th>
            <th>Date de creation</th>
            <th>Etudiant</th>
            <th>Status</th>
            <th>----</th>
          </tr>
        </thead>
        <tbody>
          {filterRequests().map((req) => (
            <tr key={req.id}>
              <td>{req.title}</td>
              <td>{req.description}</td>
              <td>{req.createdDate}</td>
              <td>{`${req.student.lastName} ${req.student.firstName}`}</td>
              <td>{req.status}</td>
              <td>
                {req.status === 'ENATTENTE' && (
                  <Button
                    variant="success"
                    block={true}
                    disabled={req.status === 'ACCEPTE'}
                    onClick={(_) => changeRequestStatus(req, true)}
                  >
                    Accepter
                  </Button>
                )}
                {req.status === 'ENATTENTE' && (
                  <Button
                    block={true}
                    variant="danger"
                    disabled={req.status === 'REJETE'}
                    onClick={(_) => changeRequestStatus(req, false)}
                  >
                    Refuser
                  </Button>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  )
}

export default ServiceRequestsList
