import React from 'react'
import Button from 'react-bootstrap/Button'
import { useHistory } from 'react-router-dom'

import '../../assets/style/Sidebar.css'

const Sidebar = () => {
  const history = useHistory()

  const route = (path) => history.push(path)

  return (
    <nav className="main-menu expanded">
      <ul>
        <li>
          <a
            onClick={(e) => {
              e.preventDefault()
              route('/admin/requests')
            }}
          >
            <i className="fa fa-home fa-2x"></i>
            <span className="nav-text">Demande de service</span>
          </a>
        </li>
        <li className="has-subnav">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault()
              route('/admin/courses')
            }}
          >
            <i className="fa fa-laptop fa-2x"></i>
            <span className="nav-text">Module / Cours</span>
          </a>
        </li>
      </ul>
    </nav>
  )
}

export default Sidebar
