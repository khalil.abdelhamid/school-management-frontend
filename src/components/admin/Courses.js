import React, { useState, useEffect } from 'react'
import CoursesAPI from '../../api/courses'
import LessonsAPI from '../../api/lessons'
import Loading from '../commons/Loading'
import Whoops from '../commons/Whoops'
import Table from 'react-bootstrap/Table'
import FormControl from 'react-bootstrap/FormControl';

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Notification from '../commons/Notification'

const Courses = () => {
  const [keyword, setKeyword] = useState('')
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState('')
  const [showNotification, setShowNotification] = useState(false)
  const [courses, setCourses] = useState([])
  const [selectedCourse, setSelectedCourse] = useState({})

  useEffect(() => {
    CoursesAPI.getAll()
      .then((res) => {
        setCourses(res)
        setSelectedCourse(res[0])
        setLoading(false)

        console.log({ courses })
        console.log({ selectedCourse })
      })
      .catch((err) => {
        console.error(err)
        setError('Falid to load data')
        setShowNotification(true)
        setLoading(false)
      })
  }, [])

  const filterCourses = () =>
    !keyword
      ? courses
      : courses.filter((course) => course.title.includes(keyword))

  const deleteLesson = (course, lesson) => {
    LessonsAPI.delete(lesson.id)
      .then((_) => {
        const newCourses = courses.map((c) => {
          if (c.id === course.id) {
            c.lessons = c.lessons.filter((l) => l.id !== lesson.id)
          }

          return c
        })
        setCourses(newCourses)
      })
      .catch((err) => {
        console.error(err)
        setError('Failed to delete lesson')
      })
  }

  return loading ? (
    <Loading />
  ) : (
    <div>
      <Container>
        <Row>
          <Col>
            <div>
              <Notification
                message={error}
                show={showNotification}
                onClose={(_) => setShowNotification(false)}
                isError={true}
              />
              <FormControl
                type="text"
                placeholder="Rechercher un module"
                value={keyword}
                className="my-2"
                onChange={(e) => setKeyword(e.target.value)}
              />
              <Table hover striped bordered>
                <tbody>
                  {filterCourses().map((course) => (
                    <tr
                      key={course.id}
                      onClick={(_) => setSelectedCourse(course)}
                    >
                      <td>
                        {course.title} ( {course.lessons.length} )
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </Col>
          <Col>
            {selectedCourse && (
              <div>
                <h1>{selectedCourse.title}</h1>
                <div className="text-secondary"> + ajouter un cours</div>
                <div>
                  {selectedCourse.lessons.map((lesson) => (
                    <div key={lesson.id}>
                      <a className="p-2" href={`/${lesson.title}`} target="_blank">
                        {lesson.title}
                      </a>
                      <span
                        style={{
                          cursor: 'pointer',
                          textDecoration: 'underline'
                        }}
                        onClick={(e) => deleteLesson(selectedCourse, lesson)}
                      >
                        Supprimer
                      </span>
                    </div>
                  ))}
                </div>
              </div>
            )}
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Courses
