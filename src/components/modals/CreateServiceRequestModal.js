import React, { useRef, useState } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import FormControl from 'react-bootstrap/FormControl'

const CreateServiceRequestModal = ({ show, onHide, onSave }) => {
  const titleRef = useRef(null)
  const descriptionRef = useRef(null)

  const [error, setError] = useState('')

  const save = () => {
    if (!titleRef.current.value || !descriptionRef.current.value) {
      return setError('Remplier les champs necessaires')
    }

    onSave(titleRef.current.value, descriptionRef.current.value)
    onHide()
  }
  return (
    <Modal show={show} centered={true} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title>Demander un service</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {error && <p className="text-danger">{error}</p>}
        <FormControl
          type="text"
          placeholder="Titre de la demande"
          ref={titleRef}
        />
        <br />
        <textarea
          style={{ width: '100%' }}
          className="p-1"
          placeholder="Description"
          ref={descriptionRef}
        ></textarea>
      </Modal.Body>
      <Modal.Footer>
        <Button vriant="primary" onClick={save}>
          Envoyer
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default CreateServiceRequestModal
