import React from 'react'

const Whoops = ({ message }) => {
  return (
    <div>
      <h1>An error has occured</h1>
      {message && <div>{message}</div>}
    </div>
  )
}

export default Whoops
