import Button from 'react-bootstrap/Button'
import React from 'react'
import { useHistory } from 'react-router-dom'
import useAuth from '../../hooks/useAuth'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

const Navbar = () => {
  const [isAuthenticated, loading, login, logout] = useAuth()
  const history = useHistory()

  const signout = () => {
    logout()
    history.push('/login')
  }

  return (
    <Container
      style={{
        marginBottom: '2rem',
      }}
    >
      <Row>
        <Col>
          <h1 style={{display: 'inline'}}>Platforme X</h1>
          <Button variant="danger" onClick={signout} style={{
            float: 'right',
            marginTop: '.5rem',
          }}>
            Sign out
          </Button>
        </Col>
      </Row>
    </Container>
  )
}

export default Navbar
