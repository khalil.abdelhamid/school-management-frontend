import React from 'react'
import '../../assets/style/404.css'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

const Notfond = () => {
  return (
    <Container className="bg-light shadow m-4 mx-auto">
      <Row className="justify-content-center">
        <Col>
          <div className="error">
            <div className="container-floud">
              <div className="col-xs-12 ground-color text-center">
                <div className="container-error-404">
                  <div className="clip">
                    <div className="shadow">
                      <span className="digit thirdDigit">4</span>
                    </div>
                  </div>
                  <div className="clip">
                    <div className="shadow">
                      <span className="digit secondDigit">0</span>
                    </div>
                  </div>
                  <div className="clip">
                    <div className="shadow">
                      <span className="digit firstDigit">4</span>
                    </div>
                  </div>
                  <div className="msg">
                    OH!<span className="triangle"></span>
                  </div>
                </div>
                <h2 className="h1">Page Introuvable</h2>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  )
}

export default Notfond
