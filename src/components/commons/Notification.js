import React from 'react'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Toast from 'react-bootstrap/Toast'

const Notification = ({ message, isError, show, onClose }) => {
  return (
    <Row style={{
      position: 'fixed',
      bottom: '1rem',
      left: '1rem',
      zIndex: '10',
    }}>
      <Col>
        <Toast onClose={() => onClose()} show={show} delay={3000} autohide>
          <Toast.Header>
            <img
              src="holder.js/20x20?text=%20"
              className="rounded mr-2"
              alt=""
            />
            <strong className="mr-auto">Notification</strong>
            <small>Maintenant</small>
          </Toast.Header>
          <Toast.Body className={isError ? 'text-danger' : 'text-primary'}>
            {message}
          </Toast.Body>
        </Toast>
      </Col>
    </Row>
  )
}

export default Notification
