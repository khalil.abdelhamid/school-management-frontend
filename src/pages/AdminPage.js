import React from 'react'
import { Switch } from 'react-router-dom'
import Navbar from '../components/commons/Navbar'
import { Route } from 'react-router-dom'
import ServiceRequestsList from '../components/admin/ServiceRequestsList'
import Courses from '../components/admin/Courses'
import Notfond from '../components/commons/404'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Sidebar from '../components/admin/Sidebar'

const AdminPage = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Navbar />
        </Col>
      </Row>
      <Row>
        <Col md={3}>
          <Sidebar />
        </Col>
        <Col className="bg-light p-4 shadow">
          <Switch>
            <Route
              exact
              path="/admin"
              component={ServiceRequestsList}
            />
            <Route
              exact
              path="/admin/requests"
              component={ServiceRequestsList}
            />
            <Route exact path="/admin/courses" component={Courses} />
            <Route path="*" component={Notfond} />
          </Switch>
        </Col>
      </Row>
    </Container>
  )
}

export default AdminPage
