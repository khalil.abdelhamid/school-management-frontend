import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Navbar from '../components/commons/Navbar'
import GradesTable from '../components/student/GradesTable'
import ServiceRequestsTable from '../components/student/ServiceRequestTable'
import AbsencesList from '../components/student/AbsencesList';
import CoursesTable from '../components/student/CoursesTable'
import Notfond from '../components/commons/404'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Sidebar from '../components/student/Sidebar'

const StudentPage = () => {
  return (
    <Container style={{position: 'relative'}}>
      <Row>
        <Col>
          <Navbar />
        </Col>
      </Row>
      <Row>
        <Col md={3}>
          <Sidebar />
        </Col>
        <Col className="bg-light p-4 shadow">
          <Switch>
            <Route exact path="/student" component={GradesTable} />
            <Route exact path="/student/grades" component={GradesTable} />
            <Route
              exact
              path="/student/requests"
              component={ServiceRequestsTable}
            />
            <Route exact path="/student/absences" component={AbsencesList} />
            <Route exact path="/student/courses" component={CoursesTable} />
            <Route path="*" component={Notfond} />
          </Switch>
        </Col>
      </Row>
    </Container>
  )
}

export default StudentPage
