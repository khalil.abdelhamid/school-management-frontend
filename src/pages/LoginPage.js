import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Login from '../components/auth/Login'

const LoginPage = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Login />
        </Col>
      </Row>
      <div className="login-bg"></div>
    </Container>
  )
}

export default LoginPage
