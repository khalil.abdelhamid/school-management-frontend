import React, { useState} from 'react'
import AuthAPI from '../api/auth'

export default function useAuth() {
  const [loading, setLoading] = useState(false)

  const isAuthenticated = () => {
    const token = localStorage.getItem('token');
    return token && token.length;
  }

  const login = (email, password) => {
    setLoading(true)
    return AuthAPI.authenticate(email, password)
      .then((t) => {
        console.log("User authenticated successfully");
        window.localStorage.setItem('token', t);
        setLoading(false);
      })
      .catch((err) => {
        console.error(err);
        setLoading(false)
      })
  }

  const logout = () => {
    console.log('signing out user');
    localStorage.removeItem('token');
  }

  return [isAuthenticated, loading, login, logout]
}
