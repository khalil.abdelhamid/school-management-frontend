import './App.css'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import Notfound from './components/commons/404'
import Login from './components/auth/Login'
import PrivateRoute from './PrivateRoute'
import AdminPage from './pages/AdminPage'
import StudentPage from './pages/StudentPage'
import Notfond from './components/commons/404'
import LoginPage from './pages/LoginPage'

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <PrivateRoute path="/admin" component={AdminPage} />
          <PrivateRoute path="/student" component={StudentPage} />
          <Route exact path="/login" component={LoginPage} />
          <Route path="/notfound" component={Notfond} />
          <Route path="*" component={Notfond} />
        </Switch>
      </Router>
    </div>
  )
}

export default App
