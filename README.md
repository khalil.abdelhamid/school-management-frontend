# TODOs

# add general route
    * [ ] add private routes and basic routing(login and registraton)
# Student
    * [x] request service (create, delete, list all requested services)
        * [x] sidebar
        * [x] navbar
        * [x] requests table
        * [x] create request
        * [x] delete request
    * [x] search for lesssons or downloads
        * [ ] download lesssons
    * [x] display grades of lessons
        * [x] search for specific lesson
    * [x] display absences of lessons
        * [ ] chart of absences
# Admin 
    * [ ] dashboard of statistic
    * [ ] add lessons to specific course
    * [x] show service requests of all students
        * [ ] validate or reject requests
        * [ ] add notes on service requests

# Backend

* [ ] request should have notes
* [ ] upload and download of file

# styling 
* [ ] use spinner in loading componenet
* [ ] use toast to display notification or error message